#adopted by adam.sida
#it crashes on GO test section
#removed perl, go and make from end of script

#################################################################
#								#
# Copyright (c) 2018-2021 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################
# See README.md for more information about this Dockerfile
# Simple build/running directions are below:
#
# Build:
#   $ docker build -t yottadb/yottadb:latest .
#
# Use with data persistence:
#   $ docker run --rm -v `pwd`/ydb-data:/data -ti yottadb/yottadb:latest

ARG OS_VSN=bullseye

# Stage 1: YottaDB build image
FROM debian:${OS_VSN} as ydb-release-builder

ARG CMAKE_BUILD_TYPE=Release
ARG DEBIAN_FRONTEND=noninteractive
#added wget, ca-certificates (gitlab tls)
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
                    file \
                    cmake \
                    make \
                    gcc \
                    pkg-config \
                    git \
                    ca-certificates \
                    wget \
                    tcsh \
                    libconfig-dev \
                    libelf-dev \
                    libgcrypt-dev \
                    libgpg-error-dev \
                    libgpgme11-dev \
                    libicu-dev \
                    libncurses-dev \
                    libssl-dev \
                    zlib1g-dev \
                    && \
    apt-get clean

#stage 2: download YottaDB source
ARG ydb_distrib=https://gitlab.com/api/v4/projects/7957109/repository/tags
ARG ydb_srcdir=/tmp/yottadb-src
ARG ydb_tmpdir=/tmp/yottadb-tmp
RUN mkdir $ydb_srcdir && \
  mkdir $ydb_tmpdir && \
  wget -P $ydb_tmpdir ${ydb_distrib} 2>&1 1>${ydb_tmpdir}/wget_latest.log
#then instead of
#ydb_version=`sed 's/,/\n/g' ${ydb_srcdir}/tags | grep -E "tag_name|.pro.tgz" | grep -B 1 ".pro.tgz" | grep "tag_name" | sort -r | head -1 | cut -d'"' -f6`
#RUN git clone --depth 1 --branch ydb_version https://gitlab.com/YottaDB/DB/YDB.git
#use this, see <https://stackoverflow.com/questions/34911622/dockerfile-set-env-to-result-of-command>
RUN sed 's/,/\n/g' ${ydb_tmpdir}/tags | grep -E "tag_name|.pro.tgz" | grep -B 1 ".pro.tgz" | grep "tag_name" | sort -r | head -1 | cut -d'"' -f6 > ./ydb_version
RUN git clone --depth 1 --branch $(cat ./ydb_version) https://gitlab.com/YottaDB/DB/YDB.git $ydb_srcdir
#cd YDB

#not necessary anymore :)
#RUN mkdir /tmp/yottadb-src
#ADD ./YDB/CMakeLists.txt /tmp/yottadb-src
# We want to copy the directories themselves, not their contents.
# Unfortunately, there is no way to do this with globs, so we have to specify each directory individually.
# c.f. <https://docs.docker.com/engine/reference/builder/#add>, <https://stackoverflow.com/questions/26504846>
#ADD ./YDB/cmake /tmp/yottadb-src/cmake
#ADD ./YDB/sr_aarch64 /tmp/yottadb-src/sr_aarch64
#ADD ./YDB/sr_armv7l /tmp/yottadb-src/sr_armv7l
#ADD ./YDB/sr_i386 /tmp/yottadb-src/sr_i386
#ADD ./YDB/sr_linux /tmp/yottadb-src/sr_linux
#ADD ./YDB/sr_port /tmp/yottadb-src/sr_port
#ADD ./YDB/sr_port_cm /tmp/yottadb-src/sr_port_cm
#ADD ./YDB/sr_unix /tmp/yottadb-src/sr_unix
#ADD ./YDB/sr_unix_cm /tmp/yottadb-src/sr_unix_cm
#ADD ./YDB/sr_unix_gnp /tmp/yottadb-src/sr_unix_gnp
#ADD ./YDB/sr_unix_nsb /tmp/yottadb-src/sr_unix_nsb
#ADD ./YDB/sr_x86_64 /tmp/yottadb-src/sr_x86_64
#ADD ./YDB/sr_x86_regs /tmp/yottadb-src/sr_x86_regs
#ADD ./YDB/.git /tmp/yottadb-src/.git

ENV GIT_DIR=/tmp/yottadb-src/.git
RUN mkdir -p /tmp/yottadb-build \
 && cd /tmp/yottadb-build \
 && test -f /tmp/yottadb-src/.yottadb.vsn || \
    grep YDB_ZYRELEASE /tmp/yottadb-src/sr_*/release_name.h \
    | grep -o '\(r[0-9.]*\)' \
    | sort -u \
    > /tmp/yottadb-src/.yottadb.vsn \
 && cmake \
      -D CMAKE_INSTALL_PREFIX:PATH=/tmp \
      -D YDB_INSTALL_DIR:STRING=yottadb-release \
      -D CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} \
      /tmp/yottadb-src \
 && make -j $(nproc) \
 && make install

# Stage 3: YottaDB release image
FROM debian:${OS_VSN} as ydb-release

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
                    file \
                    binutils \
                    ca-certificates \
                    libelf-dev \
                    libicu-dev \
                    locales \
                    wget \
                    vim \
                    procps \
                    pkg-config \
                    && \
    apt-get clean
RUN locale-gen en_US.UTF-8
WORKDIR /data
COPY --from=ydb-release-builder /tmp/yottadb-release /tmp/yottadb-release
RUN cd /tmp/yottadb-release  \
 && pkg-config --modversion icu-io \
      > /tmp/yottadb-release/.icu.vsn \
 && ./ydbinstall \
      --utf8 `cat /tmp/yottadb-release/.icu.vsn` \
      --installdir /opt/yottadb/current \
 && rm -rf /tmp/yottadb-release
ENV ydb_dir=/data \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=C.UTF-8 \
    ydb_chset=UTF-8
# MUPIP RUNDOWN need in the following chain because otherwise ENTRYPOINT complains
# about inability to run %XCMD and rundown needed. Cause not known, but this workaround works
# and is otherwise benign.
RUN . /opt/yottadb/current/ydb_env_set \
 && echo "zsystem \"mupip rundown -relinkctl\"" | /opt/yottadb/current/ydb -dir
ENTRYPOINT ["/opt/yottadb/current/ydb"]
CMD ["-run", "%XCMD", "zsystem \"bash\""]
